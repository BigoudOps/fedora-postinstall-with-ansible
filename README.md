# Fedora-postinstall-with-ansible

On [](https://flat.badgen.net/badge/icon/gnome?icon=gnome) 

![:discordapdm](https://flat.badgen.net/discord/online-members/bfB6Ve6)

![discordlinux](https://flat.badgen.net/discord/online-members/MrgtTSPS7j)

This is quick & dirty post-installation of fedora 37 workstation 

[![Join the Discord channel](https://img.shields.io/static/v1.svg?label=%20Rejoignez-nous%20sur%20Discordl&message=%F0%9F%8E%86&color=7289DA&logo=discord&logoColor=white&labelColor=2C2F33)](https://discord.gg/bfB6Ve6) 

![visitors](https://visitor-badge.glitch.me/badge?page_id=BigoudOps.readme)

[![Reddit profile](https://img.shields.io/reddit/subreddit-subscribers/apdm?style=social)](https://www.reddit.com/r/apdm) 

## Softwares inside 

- Improve DNF Performances by custom dnf.conf 😉
- Install flatpack repo
- Install media plugins
- Install some media apps ` audacity-freeworld gimp pavucontrol vlc`
- Install RpmFusion repos `free & nonfree`
- Install Gnome stuff `- dconf-editor, gnome-extensions-app, gnome-tweaks, python3-psutil, sushi`
- Copy my Vim configuration
- Install Discord client from RpmFusion
- Install Signal (messaging from flatpack)